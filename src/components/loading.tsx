import React from 'react';

const Loading = () => {
  return (
    <div className="spinner-border" role="status" style={{margin: "auto"}}>
      <span className="visually-hidden">Loading...</span>
    </div>
  );
};

export default Loading;
