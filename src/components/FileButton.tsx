import React from "react";
import { ISubmissionFile } from "../features/api/cattesterApiSlice";

const FileButton = ({file}: { file: ISubmissionFile }) => {
  const Save = () => {
    let a = document.createElement("a");
    a.href = "data:image/png;base64," + file.file;
    a.download = file.name;
    a.click()
  }
  return (
    <button type="button" className="btn btn-light me-2" onClick={Save}>{file.name}</button>
  )
}

export default FileButton;
