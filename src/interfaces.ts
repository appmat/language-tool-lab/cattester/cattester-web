export interface Problem {
  "id": number,
  "description": string,
  "lastUpdateTime": Date
}
