import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { cattesterApiSlice } from "../features/api/cattesterApiSlice";


export const store = configureStore({
  reducer: {
    [cattesterApiSlice.reducerPath]: cattesterApiSlice.reducer
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware().concat(cattesterApiSlice.middleware)
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType,
  RootState,
  unknown,
  Action<string>>;
