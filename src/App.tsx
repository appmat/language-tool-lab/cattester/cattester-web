import React from 'react';
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";

import './App.css';
import { NavBar } from "./components/NavBar";
import ViewProblem from "./features/problems/problem/ViewProblem";
import AddProblem from "./features/problems/AddProblem";
import EditProblem from "./features/problems/problem/EditProblem";
import ProblemList from "./features/problems/problemList";

function App() {

  const ProblemsPage = () => {
    return (
      <Routes>
        <Route path="/" element={<Navigate to={"/problems"}/>}/>
        <Route path="problems">
          <Route index element={<ProblemList/>}/>
          <Route path=":problemId">
            <Route index element={<ViewProblem/>}/>
            <Route path="edit" element={<EditProblem/>}/>
          </Route>
          <Route path="add" element={<AddProblem/>}/>
        </Route>
      </Routes>
    )
  }

  return (
    <div className="App">
      <BrowserRouter>
        <header className="App-header">
          <NavBar/>
        </header>
        <div className={"pt-3"} style={{height: '100%'}}>
          <div className={"list-group"}>
            <ProblemsPage/>
          </div>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
