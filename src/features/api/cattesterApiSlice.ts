import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { useCallback, useRef } from "react";
import moment from "moment";

export interface IProblem {
  "id": number,
  "description": string,
  "lastUpdateTime": Date
}

export interface INewProblem {
  "description": string
}

export interface ITest {
  "id": number,
  "index": number,
  "problemId": number,
  "input": string,
  "expectedOutput": string,
  "public": boolean
}

export interface INewTest {
  "input": string,
  "expectedOutput": string,
  "public": boolean
}

export interface ISubmission {
  "id": number,
  "problemId": number
}

export interface ISubmissionFile {
  "id": number,
  "submissionId": number,
  "name": string,
  "file": string
}

export interface ITestResult {
  "id": number,
  "testId": number,
  "submissionId": number,
  "resultStatus": "error" | "complete",
  "dateTime": Date,
  "duration": moment.Duration,
  "errorTitle": string,
  "errorOutput": string,
  "executionStatus": "running" | "completed" | "error" | "pending",
  "profileStatistics": string,
  "metrics": string
}

export const cattesterApiSlice = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({baseUrl: `${process.env.REACT_APP_API_ENDPOINT}/api`}),
  tagTypes: ['Problem', 'Test', 'Submission', 'Result'],
  refetchOnMountOrArgChange: 5,
  endpoints: (builder) => ({
    getProblems: builder.query<IProblem[], void>({
      query: () => '/problems',
      providesTags: (result: IProblem[] = []) => [
        {type: 'Problem' as const, id: 'LIST'},
        ...result.map(({id}) => ({type: 'Problem' as const, id}))
      ]
    }),
    getProblem: builder.query<IProblem, { problemId: number }>({
      query: ({problemId}) => `/problems/${problemId}`,
      providesTags: (result, error, {problemId}) => [{type: 'Problem', problemId}]
    }),
    addProblem: builder.mutation<void, { newProblem: INewProblem }>({
      query: ({newProblem}) => ({
        url: '/problems',
        method: 'POST',
        body: newProblem
      }),
      invalidatesTags: ['Problem']
    }),
    updateProblem: builder.mutation<void, { problemId: number, problem: INewProblem }>({
      query: ({problemId, problem}) => ({
        url: `/problems/${problemId}`,
        method: 'PUT',
        body: problem
      }),
      invalidatesTags: ['Problem']
    }),
    getTests: builder.query<ITest[], { problemId: number }>({
      query: ({problemId}) => `/problems/${problemId}/tests`,
      providesTags: (result: ITest[] = []) => [
        {type: 'Test' as const, id: 'LIST'},
        ...result.map(({id}) => ({type: 'Test' as const, id}))
      ]
    }),
    getTest: builder.query<ITest, { problemId: number, testId: number }>({
      query: ({problemId, testId}) => `/problems/${problemId}/tests/${testId}`,
      providesTags: (result, error, {testId}) => [{type: 'Test', id: testId}]
    }),
    addTest: builder.mutation<void, { problemId: number, newTest: INewTest }>({
      query: ({problemId, newTest}) => ({
        url: `/problems/${problemId}/tests`,
        method: 'POST',
        body: newTest
      }),
      invalidatesTags: ['Test']
    }),
    updateTest: builder.mutation<void, { problemId: number, testId: number, test: INewTest }>({
      query: ({problemId, testId, test}) => ({
        url: `/problems/${problemId}/tests/${testId}`,
        method: 'PUT',
        body: test
      }),
      invalidatesTags: ['Test']
    }),
    deleteTest: builder.mutation<void, { problemId: number, testId: number }>({
      query: ({problemId, testId}) => ({
        url: `/problems/${problemId}/tests/${testId}`,
        method: 'DELETE'
      }),
      invalidatesTags: ['Test']
    }),
    getSubmissionList: builder.query<ISubmission[], { problemId: number }>({
      query: ({problemId}) => `/problems/${problemId}/submissions`,
      providesTags: (result: ISubmission[] = []) => [
        {type: 'Submission' as const, id: 'LIST'},
        ...result.map(({id}) => ({type: 'Test' as const, id}))
      ]
    }),
    addSubmission: builder.mutation<void, { problemId: number, formData: FormData }>({
      query: ({problemId, formData}) => {
        console.log(Array.from(formData.entries()))
        return {
          url: `/problems/${problemId}/submissions`,
          method: 'POST',
          body: formData
        }
      },
      invalidatesTags: ["Submission"]
    }),
    getSubmissionFileList: builder.query<ISubmissionFile[], { problemId: number, submissionId: number }>({
      query: ({problemId, submissionId}) => ({
        url: `/problems/${problemId}/submissions/${submissionId}/submissionFiles`,
        method: 'GET'
      }),
      // transformResponse: (response: {data: ISubmissionFile[]}) => {
      //   response.data.map((submissionFile): ISubmissionFile => ({
      //     ...submissionFile,
      //     file: new Blob(submissionFile.file)
      //   }))
      // }
    }),
    getSubmissionFile: builder.query<ISubmissionFile, { problemId: number, submissionId: number, fileId: number }>({
      query: ({problemId, submissionId, fileId}) => ({
        url: `/problems/${problemId}/submissions/${submissionId}/submissionFiles/${fileId}`,
        method: 'GET'
      })
    }),
    getTestResultList: builder.query<ITestResult[], { problemId: number, submissionId: number }>({
      query: ({problemId, submissionId}) => ({
        url: `/problems/${problemId}/submissions/${submissionId}/testResults`,
        method: 'GET'
      }),
      keepUnusedDataFor: 5,
      async onCacheEntryAdded(arg, api) {
        await api.cacheDataLoaded
        if (api.getCacheEntry().data?.find(test => test.executionStatus === "pending" || test.executionStatus === "running")) {
          const source = new EventSource(`${process.env.REACT_APP_API_ENDPOINT}/api/problems/${arg.problemId}/submissions/${arg.submissionId}/testResults/SSE`);

          source.onmessage = (e) => {
            try {
              const data: ITestResult[] = JSON.parse(e.data)
              api.updateCachedData((draft) => {
                let allFinish = true
                data.forEach(update => {
                  draft[draft.findIndex(res => res.id === update.id)].executionStatus = update.executionStatus
                  allFinish = allFinish && (update.executionStatus === "completed" || update.executionStatus === "error")
                })
                allFinish && source.close()
              })
            } catch (e) {
              console.error(e)
            }
          }

          source.onopen = () => {
            console.log('SSE open');
          }

          source.onerror = (e) => {
            console.error('SSE error');
            console.error(e);
          }

          await api.cacheEntryRemoved
          source.close()
        }
      }
    }),
  })
})

export const {
  useGetProblemsQuery,
  useGetProblemQuery,
  useLazyGetProblemQuery,
  useAddProblemMutation,
  useUpdateProblemMutation,
  useGetTestsQuery,
  useAddTestMutation,
  useUpdateTestMutation,
  useDeleteTestMutation,
  useGetSubmissionListQuery,
  useGetSubmissionFileListQuery,
  useAddSubmissionMutation,
  useGetTestResultListQuery
} = cattesterApiSlice


export const useGetProblem = ({problemId}: { problemId: number }): {
  problem: IProblem | undefined,
  isLoading: boolean,
  isError: boolean
} => {
  const {data: problem, isLoading, isError} = cattesterApiSlice.useGetProblemQuery({problemId})
  const {data: problems} = cattesterApiSlice.endpoints.getProblems.useQueryState(undefined)

  const getCachedProblem = useCallback((): IProblem | undefined => {
    return problem || problems?.find((problem) => problem.id === problemId)
  }, [problem, problemId, problems])

  const problemResult = useRef<IProblem | undefined>()
  problemResult.current = getCachedProblem()

  return {problem: problemResult.current, isLoading, isError}
}
