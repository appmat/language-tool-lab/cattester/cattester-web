import * as React from 'react';
import { Link } from "react-router-dom";

import { Problem } from "../../interfaces";


type Props = {
  problemData: Problem
};

const ProblemListItem: React.FC<Props> = ({problemData}) => {
  const GetLastUpdateTime = () => {
    let date = new Date(problemData.lastUpdateTime)
    let now = new Date(Date.now())
    if (date.toLocaleDateString() === now.toLocaleDateString()){
      return date.toLocaleTimeString()
    } else {
      return date.toLocaleDateString()
    }
  }

  return (
    <Link className={"list-group-item border-0 px-0"} to={`/problems/${problemData.id}`}>
      <div className="my-3 p-2 rounded-3 border shadow-sm">
        <div className="px-2 pb-2 mb-2 border-bottom d-flex justify-content-between align-items-center">
          <span className="ps-5 fw-bolder fs-5">Задача {problemData.id}</span>
        </div>

        <div className="px-2 py-2 border-bottom" role="button">{problemData.description}</div>

        <div className="pt-2 px-2 d-flex justify-content-between align-items-center">
          <span className="fst-italic">Обновлено: {GetLastUpdateTime()}</span>
        </div>
      </div>
    </Link>
  );
};

export default ProblemListItem;
