import React from 'react';
import { Link } from "react-router-dom";

import ProblemListItem from "./problemListItem";
import { useGetProblemsQuery } from "../api/cattesterApiSlice";
import Loading from "../../components/loading";

const ProblemList = () => {
  const {data: problems, isLoading, isError} = useGetProblemsQuery()

  if (isError) return <div>An error has occurred!</div>

  if (isLoading) return <Loading/>

  return (
    <div className="container">
      <Link to={"/problems/add"} className={"d-flex justify-content-end mb-2"}>
        <button className="btn btn-primary">Создать задачу</button>
      </Link>
      {problems!.length
        ? Array.from(problems || []).sort((p1, p2) => p1.id - p2.id).map(prob => (
          <ProblemListItem problemData={prob} key={prob.id}/>
        ))
        : <p>Нет задач</p>}
    </div>
  );
};

export default ProblemList;
