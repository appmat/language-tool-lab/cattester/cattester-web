import React, { useState } from 'react';
import ReactMarkdown from 'react-markdown'

import { useNavigate } from "react-router-dom";
import { useAddProblemMutation } from "../api/cattesterApiSlice";

const AddProblem = () => {
  const [addNewProblem, {isLoading, isError}] = useAddProblemMutation()

  const navigate = useNavigate();

  const [description, setDescription] = useState("");

  const onDescriptionChanged = (e: React.ChangeEvent<HTMLTextAreaElement>) => setDescription(e.target.value)

  const canSave = [description].every(Boolean) && !isLoading

  const onSubmit = async (e: React.SyntheticEvent) => {
    e.preventDefault()
    if (canSave) {
      try {
        await addNewProblem({newProblem: {description}}).unwrap()
        if (!isLoading && !isError )
          navigate(`/problems`)
      } catch (err) {
        console.error('Failed to save the post: ', err)
      }
    }
  }

  return (
    <div className="container">
      <form onSubmit={onSubmit}>
        <div className="mb-3">
          <div className="d-flex justify-content-between mb-2">
            <label htmlFor="exampleFormControlTextarea1" className="form-label align-self-end">Описание задачи</label>
            <button type="submit" className="btn btn-primary"
                    disabled={!canSave}>{isLoading ? 'Отправка...' : 'Сохранить'}</button>
          </div>
          <textarea className="form-control" id="exampleFormControlTextarea1" rows={3} onChange={onDescriptionChanged}/>
        </div>
      </form>
      <div className="border border-dark rounded p-2 bottom-0 overflow-auto text-break d-flex align-items-stretch">
        <ReactMarkdown children={description}/>
      </div>
    </div>
  );
}

export default AddProblem;
