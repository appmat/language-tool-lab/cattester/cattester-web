import React from 'react';
import { ITest } from "../../../api/cattesterApiSlice";

type Props = {
  test: ITest,
  editTestHandler(): void
}

const TestListItem: React.FC<Props> =({test, editTestHandler}) => {
  return (
    <div className="my-3 p-2 rounded-3 border shadow-sm">
      <div className="px-2 pb-2 mb-2 border-bottom d-flex justify-content-between align-items-center">
        <div>
          {test.public ? <i className="bi bi-eye fs-5"/> : <i className="bi bi-eye-slash-fill fs-5"/>}
          <span className="ps-2 fs-5">Тест {test.index}</span>
        </div>

        <div className="btn btn-outline-dark py-1 px-4" title="Редактировать" onClick={editTestHandler}>
          <i className="bi bi-pencil-square"/>
        </div>
      </div>

      <div className="px-2 d-flex align-items-center">
        <i className="bi bi-arrow-right-square fs-4 pe-2"/>
        <span className="fst-italic fw-bold"/>
        <span className="overflow-auto">{test.input}</span>
      </div>
      <div className="px-2 d-flex align-items-center">
        <i className="bi bi-arrow-left-circle-fill fs-4 pe-2"/>
        <span className="fst-italic fw-bold"/>
        <span className="overflow-auto">{test.expectedOutput}</span>
      </div>

    </div>
  )
}

export default TestListItem;
