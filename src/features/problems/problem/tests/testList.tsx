import React, { useState } from 'react';
import { useParams } from "react-router-dom";
import {
  INewTest,
  ITest,
  useAddTestMutation,
  useGetTestsQuery,
  useUpdateTestMutation
} from "../../../api/cattesterApiSlice";
import TestItemEditorModal from "./testItemEditorModal";
import TestListItem from "./testListItem";
import Loading from "../../../../components/loading";

interface IModal {
  mode: 'none' | 'new' | 'edit',
  test?: ITest
}

const TestList = () => {
  const [addTest] = useAddTestMutation()
  const [updateTest] = useUpdateTestMutation()

  let params = useParams()
  const problemId: number = +params.problemId!;

  const [modal, setModal] = useState<IModal>({mode: 'none'})

  const {data: tests, isLoading} = useGetTestsQuery({problemId})

  const saveNewTest = (problemId: number, newTest: INewTest) => {
    addTest({problemId, newTest})
  }

  const saveTest = (test: ITest) => {
    updateTest({problemId: test.problemId, testId: test.id, test})
  }

  const onClickAdd = () => {
    setModal({mode: 'new'})
  }

  const onClickEdit = (test: ITest) => {
    setModal({mode: 'edit', test})
  }

  const closeModal = () => {
    setModal({mode: 'none'})
  }

  if (isLoading)
    return <Loading/>

  const modalSwitch = () => {
    switch (modal.mode) {
      case "edit":
        return <TestItemEditorModal oldTest={modal.test} submitTestHandler={saveTest} closeHandler={closeModal}/>
      case "new":
        return <TestItemEditorModal submitTestHandler={saveNewTest.bind(undefined, problemId)}
                                    closeHandler={closeModal}/>
      case "none":
        return null
    }
  }

  return (
    <div className={""}>
      <div className="px-2 pb-2 my-2 border-bottom d-flex justify-content-between align-items-center">
        <span className="fw-bolder fs-4">Тесты</span>
        <div className="btn btn-warning" title="Редактировать" onClick={onClickAdd}><span> Добавить тест</span></div>
      </div>
      {tests && tests.length ? tests.map(test => (
        <TestListItem test={test} key={test.id} editTestHandler={onClickEdit.bind(undefined, test)}/>
      )) : <p>Нет тестов</p>}
      {modalSwitch()}
    </div>
  )
}

export default TestList;
