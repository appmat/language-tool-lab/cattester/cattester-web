import React, { useEffect, useRef, useState } from 'react';
import { INewTest, ITest } from "../../../api/cattesterApiSlice";

type Props = {
  oldTest?: ITest,
  submitTestHandler(test: ITest | INewTest): void,
  closeHandler(): void
}

const TestListItemEditaNew = ({oldTest, submitTestHandler, closeHandler}: Props) => {
  const isNew = oldTest === undefined
  const [show, setShow] = useState('')
  const formRef = useRef<HTMLFormElement>(null)

  useEffect(() => {
    setShow('show')
  }, [])

  const onClose = async (e?: React.MouseEvent<HTMLElement>) => {
    // @ts-ignore
    if (e === undefined || e.target.id === 'modalCenter') {
      await
      setShow('')
      setTimeout(closeHandler, 300)
    }
  }

  const onClickSave = () => {
    if (formRef.current)
      submitTestHandler(getFormData(formRef.current))
      onClose()
  }

  const getFormData = (element: HTMLFormElement): ITest | INewTest => {
    let formData = new FormData(element)
    return  {
      ...oldTest,
      input: formData.get("input") as string,
      expectedOutput: formData.get("expectedOutput") as string,
      public: formData.has("public")
    }
  }

  const onFormChange = (e: React.FormEvent<HTMLFormElement>) => {
    if (!isNew)
      submitTestHandler(getFormData(e.currentTarget))
  }

  return (
    <>
      <div className={"modal fade " + show} id="modalCenter" role="dialog" tabIndex={-1}
           aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style={{display: 'block'}} onClick={onClose}>
        <div className="modal-dialog modal-dialog-centered" role="document" id="window">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLongTitle">
                {oldTest ? `Тест ${oldTest.index}` : 'Новый тест'}
              </h5>
            </div>
            <div className="modal-body">
              <form className={"mt-3 "} onChange={onFormChange} ref={formRef}>
                <label htmlFor={"input"}>Входные данные:</label>
                <input className="form-control mb-2" id="input" autoComplete="off" name="input"
                       defaultValue={oldTest?.input}/>
                <label htmlFor={"expectedOutput"}>Ожидаемые выходные данные:</label>
                <input className="form-control mb-2" id="expectedOutput"
                       autoComplete="off" name="expectedOutput"
                       defaultValue={oldTest?.expectedOutput}/>
                <input className="form-check-input" type="checkbox" value="" id="isPublicCheckBox" name="public"
                       defaultChecked={oldTest?.public}/>
                <label className="ps-1 form-check-label" htmlFor="isPublicCheckBox">
                  Публичный тест
                </label>
              </form>
            </div>
            {isNew ?
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" onClick={onClickSave}>Сохранить</button>
              </div> : null
            }
          </div>
        </div>
      </div>
    </>
  )
}

export default TestListItemEditaNew;
