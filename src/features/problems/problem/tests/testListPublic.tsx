import React from 'react';
import { useParams } from "react-router-dom";
import { ITest, useGetTestsQuery } from "../../../api/cattesterApiSlice";


const TestListPublic = () => {
  let params = useParams()
  const problemId: number = +params.problemId!;

  const {data: tests} = useGetTestsQuery({problemId})

  const Test = ({test}: { test: ITest }) => (
    <div className={"mt-3"}>
      <span>Тест {test.index}</span>
      <div className={"border border-dark rounded p-2 bottom-0 overflow-auto text-break d-flex align-items-stretch"}>
        <code>
          Ввод: {test.input}
          <br/>
          Вывод: {test.expectedOutput}
        </code>
      </div>
    </div>
  )

  return (
    <div className={""}>
      {
        tests && tests.length ?
          tests.map(test =>
            test.public ? (
              <Test test={test} key={test.id}/>
            ) : null
          ) : <p>Нет тестов</p>
      }
    </div>
  )
}

export default TestListPublic;
