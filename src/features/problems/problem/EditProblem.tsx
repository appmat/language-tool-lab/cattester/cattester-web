import React, { useEffect, useState } from 'react';
import ReactMarkdown from 'react-markdown'

import { useParams } from "react-router-dom";
import { useGetProblem, useUpdateProblemMutation } from "../../api/cattesterApiSlice";
import TestList from "./tests/testList";
import remarkMath from 'remark-math'
import rehypeKatex from 'rehype-katex'
import 'katex/dist/katex.min.css';
import Loading from "../../../components/loading";

const EditProblem = () => {
  let params = useParams()
  const [updateNewProblem, {isLoading: isUploading, isError: isUploadingError}] = useUpdateProblemMutation()
  const problemId: number = +params.problemId!;
  const {problem, isLoading: isLoadingProblem} = useGetProblem({problemId})
  const [description, setDescription] = useState(problem?.description);

  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    if (!isLoadingProblem && isLoading) {
      setIsLoading(false)
      setDescription(problem?.description)
    }
  }, [isLoading, isLoadingProblem, problem?.description])

  if (problem === undefined) {
    if (isLoading)
      return <Loading/>
    else
      return <p>Задача не найдена</p>
  }

  const onDescriptionChanged = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setDescription(e.target.value)
    updateNewProblem({problemId, problem: {description: e.target.value}})
  }

  return (
    <div className="container">
      <div className={"mb-3"}>
        <form>
          <div className="mb-3">
            <div className="d-flex justify-content-between mb-2 align-items-center">
              <h3 className={"mb-0"}>
                <label htmlFor="exampleFormControlTextarea1" className="form-label align-self-end">Описание
                  задачи</label>
              </h3>
              <span>{isUploading ? "Сохранение..." : (isUploadingError ? "Ошибка" : "готово")}</span>
              {/*<button type="submit" className="btn btn-primary">Сохранить</button>*/}
            </div>
            <textarea className="form-control" id="exampleFormControlTextarea1" rows={3} onChange={onDescriptionChanged}
                      value={description}/>
          </div>
        </form>
        <div className="border border-dark rounded p-2 bottom-0 overflow-auto text-break d-flex align-items-stretch">
          <div>
            <ReactMarkdown children={description!} remarkPlugins={[remarkMath]} rehypePlugins={[rehypeKatex]}/>
          </div>
        </div>
      </div>
      <TestList/>
    </div>
  );
}

export default EditProblem;
