import React from 'react';
import { Link, useParams } from "react-router-dom";
import ReactMarkdown from 'react-markdown'
import { useGetProblem } from "../../api/cattesterApiSlice";
import TestListPublic from "./tests/testListPublic";
import remarkMath from 'remark-math'
import rehypeKatex from 'rehype-katex'
import 'katex/dist/katex.min.css';
import Loading from "../../../components/loading";
import AddSubmission from "./submissions/AddSubmission";
import SubmissionList from "./submissions/submissionList";


const ViewProblem = () => {
  const params = useParams();
  const problemId = +params.problemId!

  const {problem, isLoading} = useGetProblem({problemId})

  if (problem === undefined) {
    if (isLoading)
      return <Loading/>
    else
      return <p>Задача не найдена</p>
  }

  return (
    <div className="container mb-3">
      <div className="d-flex justify-content-between mb-2">
        <label htmlFor="description-field" className="form-label align-self-end">Описание задачи</label>
        <Link to={"edit"} className={"d-flex justify-content-end mb-2"}>
          <button className="btn btn-primary">Изменить</button>
        </Link>
      </div>
      <div className="border border-dark rounded p-2 bottom-0 overflow-auto text-break d-flex align-items-stretch">
        <div>
          <ReactMarkdown children={problem.description} remarkPlugins={[remarkMath]} rehypePlugins={[rehypeKatex]}/>
        </div>
      </div>
      <TestListPublic/>
      <AddSubmission problemId={problemId}/>
      <SubmissionList/>
    </div>
  )
}

export default ViewProblem;
