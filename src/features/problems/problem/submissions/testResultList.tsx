import React from 'react';
import {
  ITestResult,
  useGetTestResultListQuery,
} from "../../../api/cattesterApiSlice";
import Loading from "../../../../components/loading";

type Props = {
  problemId: number,
  submissionId: number
}

const TestResultList: React.FC<Props> = ({problemId, submissionId}) => {
  const {data: testResultList, isLoading: isLoadingTestResultList} = useGetTestResultListQuery({problemId, submissionId})

  if (isLoadingTestResultList)
    return <Loading/>

  const statusIcon = ({testResult} : {testResult: ITestResult}): JSX.Element => {
    switch (testResult.executionStatus) {
      case "completed":
        return <i className="bi bi-check-circle fs-4 ms-2"/>
      case "error":
        return <i className="bi bi-x-circle fs-4 ms-2"/>
      case "running":
        return <i className="bi bi-play-circle fs-4 ms-2"/>
      case "pending":
        return <i className="bi bi-stop-circle fs-4 ms-2"/>
    }
  }

  return (
    <>
      {testResultList?.map(testResult => (
        <div className="d-flex flex-row" key={testResult.id}>
          {statusIcon({testResult})}
        </div>
      ))}
    </>
  );
}

export default TestResultList;
