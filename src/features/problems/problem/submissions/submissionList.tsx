import React from 'react';
import { useGetSubmissionListQuery } from "../../../api/cattesterApiSlice";
import { useParams } from "react-router-dom";
import Loading from "../../../../components/loading";
import SubmissionListItem from "./submissionListItem";

const SubmissionList = () => {
  let params = useParams()
  const problemId: number = +params.problemId!;
  const {data: submissions, isLoading} = useGetSubmissionListQuery({problemId});

  if (isLoading)
    return <Loading/>

  return (
    <div className={"accordion mt-4"}>
      {Array.from(submissions || []).sort((s1, s2) => s2.id - s1.id).map((submission) => (
        <SubmissionListItem problemId={problemId} submissionId={submission.id} key={submission.id}/>
      ))}
    </div>
  );
}

export default SubmissionList;
