import React, { useRef } from 'react';
import { useAddSubmissionMutation } from "../../../api/cattesterApiSlice";

type Props = {
  problemId: number
}

const AddSubmission: React.FC<Props> = ({problemId}) => {
  const [addSubmission, {isLoading}] = useAddSubmissionMutation()
  const formData = useRef(new FormData())

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (formData.current.has("file0")){
      addSubmission({problemId, formData: formData.current})
      e.currentTarget.reset()
      formData.current = new FormData()
    }
  }

  const onChange = (e: React.ChangeEvent<HTMLFormElement>) => {
    formData.current = new FormData()
    Array.from(e.target.files).forEach((val, i) => {
      formData.current.append(`file${i}`, val as string | Blob)
    })
  }

  return (
    <form onSubmit={onSubmit} onChange={onChange} className="mb-2 mt-3">
      <label htmlFor="formFileMultiple" className="form-label">Создать новое решение</label>
      <input className="form-control" type="file" id="formFileMultiple" multiple draggable={"true"}/>
      <button type="submit" className="btn btn-primary mt-2" disabled={isLoading}>Отправить решение</button>
    </form>
  );
}

export default AddSubmission;
