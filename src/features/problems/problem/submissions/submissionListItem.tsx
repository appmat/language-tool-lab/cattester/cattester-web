import React from 'react';
import {
  useGetSubmissionFileListQuery,
} from "../../../api/cattesterApiSlice";
import Loading from "../../../../components/loading";
import FileButton from "../../../../components/FileButton";
import TestResultList from "./testResultList";

type Props = {
  problemId: number,
  submissionId: number
}

const SubmissionListItem: React.FC<Props> = ({problemId, submissionId}) => {
  const {data: fileList, isLoading: isLoadingSubmissionList} = useGetSubmissionFileListQuery({problemId, submissionId})

  if (isLoadingSubmissionList)
    return <Loading/>

  return (
    <div className="accordion-item">
      <h2 className="accordion-header" id={`heading${submissionId}`}>
        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target={`#collapse${submissionId}`}
                aria-expanded="false" aria-controls={`collapse${submissionId}`}>
          <span>{submissionId}. </span>
          <TestResultList problemId={problemId} submissionId={submissionId}/>
        </button>
      </h2>
      <div id={`collapse${submissionId}`} className="accordion-collapse collapse" aria-labelledby={`heading${submissionId}`}
           data-bs-parent="#accordionExample">
        <div className="accordion-body">
          {fileList?.length
            ? fileList.map(file => (
              <FileButton file={file} key={file.id}/>
            ))
            : "Странно... Для этого решения нет файлов"
          }
        </div>
      </div>
    </div>
  );
}

export default SubmissionListItem;
